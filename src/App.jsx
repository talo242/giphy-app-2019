import React, { useState, useEffect } from 'react';
import './App.css';
import Header from './components/Header';
import Searchbox from './components/Searchbox';
import Gif from './components/Gif';

/**
 * 1. Hacer una consulta al API de Giphy
 * 2. Guardar la respuesta en el estado
 * 3. Renderizar los gifs en la UI
 *
 * TODO:
 * 1. Capturar el input del usuario y guardarla en el estado
 * 2. Al hacer enter, hacer consultar el API de Giphy con el query
 * 3. Guardar la respuesta en el estado
 * 4. Renderizar los gifs en la UI
 */

const baseUrl = 'https://api.giphy.com/v1/gifs';
const apiKey = '7YIJEiCrZjNszVSJeXD5JkzA9Ud7dJCG';

function App() {
  const [gifs, setGifs] = useState([]);
  const [query, setQuery] = useState('');
  const [currentGif, setCurrentGif] = useState(null);

  const apiCall = url => {
    fetch(url)
      .then(response => response.json())
      .then(response => {
        setGifs(response.data);
      });
  };

  useEffect(() => {
    apiCall(`${baseUrl}/trending?api_key=${apiKey}&limit=25&rating=G`);
  }, []);

  const searchGif = e => {
    e.preventDefault();
    apiCall(
      `${baseUrl}/search?api_key=${apiKey}&q=${query}&limit=25&offset=0&rating=G&lang=en`,
    );
    setQuery('');
  };

  const handleRandomGif = () => {
    fetch(`${baseUrl}/random?api_key=${apiKey}&tag=&rating=G`)
      .then(response => response.json())
      .then(response => {
        setGifs(gifs.concat(response.data));
      });
  };

  const handleCloseModal = () => {
    setCurrentGif(null);
  }

  const handleSelectGif = (elemento) => {
    console.log(elemento);
    setCurrentGif(elemento);
  }

  return (
    <div className="App">
      <div className="container">
        <Header />
        <Searchbox
          onSubmit={searchGif}
          value={query}
          onChange={event => setQuery(event.target.value)}
        />
        <div className="gif-list">
          {gifs.length > 0
            ? gifs.map(elemento => {
                return (
                  <Gif
                    key={elemento.id}
                    source={elemento.images.original.url}
                    onClick={() => handleSelectGif(elemento)}
                  />
                );
              })
            : 'Cargando...'}
        </div>
        <button className="header-button" onClick={handleRandomGif}>
          Cargar más
        </button>
      </div>
      {
        currentGif && (
          <div className="modal-container">
            <div className="modal">
              <div className="modal-header">
                <h1>{currentGif.title}</h1>
                <button onClick={handleCloseModal}>x</button>
              </div>
              {currentGif.username && <p>Username: {currentGif.username}</p>}
              <p>URL: {currentGif.url}</p>
              <img src={currentGif.images.original.url} />
            </div>
          </div>
        )
      }
    </div>
  );
}

export default App;
