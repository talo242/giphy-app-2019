import React from 'react';
import '../css/Title.css';

function Age(props) {
  return <p>{props.age}</p>
}

function Title(props) {
  return (
    <div className="title-container">
      <h1>{props.text}</h1>
      {
        props.age ? <Age age={props.age} /> : null
      }
    </div>
  );
}

export default Title;
