import React from 'react';

function SearchBox(props) {
  const { onSubmit, onChange, value } = props;

  return (
    <form onSubmit={onSubmit}>
      <input
        className="search-input"
        type="text"
        placeholder="Buscar Gif"
        value={value}
        onChange={onChange}
      />
    </form>
  );
}

export default SearchBox;
