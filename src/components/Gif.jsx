import React from 'react';

function Gif(props) {
  const { source, onClick } = props;
  return (
    <div className="gif-container" onClick={onClick}>
      <img className="gif" src={source} />
    </div>
  );
}

export default Gif;
